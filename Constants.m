

CGFloat const kLeftMargin = 20.0f;
CGFloat const kTopMargin  = 20.0f;
CGFloat const kRightMargin = 20.0f;
CGFloat const kTweenMargin	= 6.0f;

CGFloat const kTextFieldHeight = 30.0f;
